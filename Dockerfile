FROM node:carbon

WORKDIR /app

EXPOSE 3000

COPY package*.json ./
RUN npm install

COPY . .

CMD [ "node", "index" ]