# Pusher

Simple self-hosted deployment automation.

## Usage

Pusher deployments are triggered by **hooks** that are defined in the `hooks.json` file. Here is an example:

```json
{
  "example_hook": {
    "script": "script.sh",
    "secret": "shhhh_this_is_a_secret",
    "gitlab": {
      "token": "gitlab_personal_access_token",
      "host": "optional_host",
      "project": 123456
    },
    "telegram": {
      "token": "your_bot_token",
      "user": 123456
    }
  }
}
```

### Hook Configuration Options

- `script` - The script (in the `scripts` directory) to run when the hook is executed
- `secret` - **Optional.** A secret that must be passed when the hook is run
- `gitlab` - **Optional.** Defines a GitLab integration for this hook
  - `token` - Your GitLab personal access token
  - `host` - **Optional.** If you are using a different GitLab instance, put the host's URL here
  - `project` - The project ID that this hook is attached to
- `telegram` - **Optional.** Defines a Telegram integration for this hook
  - `token` - Your bot's API token
  - `user` - User ID to send job updates to

### GitLab Integration

Pusher has a GitLab integration. To use it, go to your project and add an integration in Settings.

For example, if you were hosting a Pusher instance on `pusher.example.com` and wanted to trigger our example hook above (`example_hook`), you could use this as the URL:

```
https://pusher.example.com/hooks/example_hook
```

If your hook has a secret (our example does), you can put that in the "Secret Token" field. Pusher will look for the secret in the `X-Gitlab-Token` header or `secret` query parameter.

Every time you push to GitLab and a Pusher hook is triggered, the status will be reflected on the commit's page under the "Pipelines" tab. It will look something like this:

![](https://i.imgur.com/KHJwPVK.png)

### Telegram Integration

You can get notified via Telegram when your hooks are executed. See the `telegram` configuration option above.

The user ID specified will be notified when the job starts, fails, or finishes.

### Triggering Hooks

Pusher has a web server that listens on port `$PORT` or `3000`. To trigger a hook, send a POST to `/hook/:hook_name`. If your hook has a secret, send it as a `secret` query parameter.

Following our example above, our full URL should look something like this:

```
https://pusher.example.com/hooks/example_hook?secret=shhhh_this_is_a_secret
```

### Running Pusher

Before you run Pusher, you should define your hooks in `hooks.json`. A sample `hooks.json` is provided (`hooks.example.json`) to get you started.

#### Docker (recommended)

You can run Pusher with the pre-built Docker image or build from source. A sample `docker-compose.yml` is provided in the root of this repository to get you started:

```yaml
version: '3'
services:
  web:
    image: tjhorner/pusher:latest # Comment this line and uncomment the below line if you want to build the image from source.
    # build: .
    ports:
      - "19293:3000" # Listen on host port 19293. You can change this to anything you want.
    volumes: # You probably want to keep these volumes here
      - "./hooks.json:/app/hooks.json"
      - "./scripts:/app/scripts"
```

#### Manual

You can run Pusher if you have Node.js installed:

```shell
npm install # Install dependencies
# Remember to set up your hooks in hooks.json first!
node index # Run Pusher. It is now listening on $PORT or 3000.
```

## Why?

[Some automated deployment solutions](https://gitlab.com/help/ci/quick_start/README) are too complex when all you want to do is run a script on a remote machine upon pushing to your repo. Pusher makes that easy. Just define your hooks and run it. Tada!

## License

Pusher is licensed under the GNU GPL, version 3.0. A copy of the license is in the `LICENSE` file.