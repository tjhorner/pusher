const express = require('express')
const hooks = require('./hooks.json')
const bodyParser = require('body-parser')
const Job = require('./job')
const GitLabTrigger = require('./integrations/gitlab')
const TelegramTrigger = require('./integrations/telegram')
const web = express()

web.use(bodyParser.json({ extended: true }))

var createJob = (hook, req, res) => {
  var triggers = [ ]

  if(hook.gitlab)
    triggers.push(new GitLabTrigger(hook.gitlab.token, hook.gitlab.project, req.body.checkout_sha, hook.gitlab.host))

  if(hook.telegram)
    triggers.push(new TelegramTrigger(hook.telegram.token, hook.telegram.user))

  var job = new Job(hook, triggers, true)
  res.sendStatus(200)
}

web.post("/hook/:hook", (req, res) => {
  if(hooks[req.params.hook]) {
    var hook = hooks[req.params.hook]
    hook.name = req.params.hook

    if(hook.secret && (req.query.secret === hook.secret || req.headers["x-gitlab-token"] === hook.secret)) {
      createJob(hook, req, res)
    } else if(!hook.secret) {
      createJob(hook, req, res)
    } else {
      res.sendStatus(401)
    }
  } else {
    res.sendStatus(404)
  }
})

web.listen(process.env.PORT || 3000)