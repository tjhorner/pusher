// GitLab Integration
const GitLab = require('gitlab/dist/es5').default

class GitLabTrigger {
  constructor(token, project, commit, url) {
    this.token = token
    this.url = url || "https://gitlab.com"
    this.project = project
    this.commit = commit
    this.gitlab = new GitLab({
      url: this.url, token
    })
  }

  update(status) {
    this.gitlab.Commits.editStatus(this.project, this.commit, { state: status, name: "Pusher Deployment" })
  }
}

module.exports = GitLabTrigger