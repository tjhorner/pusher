// Telegram Integration
const Telegram = require('telegraf/telegram')

class TelegramTrigger {
  constructor(token, user) {
    this.token = token
    this.user = user
    this.telegram = new Telegram(token)
  }

  update(status, job) {
    var message = ""

    switch(status) {
      case "running":
        message = `⚪️ *Running Pusher hook ${job.hook.name}*`
        break
      case "failed":
        message = `🔴 *Pusher hook ${job.hook.name} failed!*`
        break
      case "success":
        message = `🔵 *Pusher hook ${job.hook.name} successful*`
        break
    }

    this.telegram.sendMessage(this.user, message, { parse_mode: "Markdown" })
  }
}

module.exports = TelegramTrigger