const EventEmitter = require('events')
const path = require('path')
const execFile = require('child_process').execFile

class Job extends EventEmitter {
  constructor(hook, triggers, startImmediately = false) {
    super()
    this.hook = hook
    this.triggers = triggers

    if(startImmediately)
      this.start()
    else
      this.updateStatus("pending")
  }

  updateStatus(status) {
    this.status = status
    this.emit("status", status)
    this.triggers.forEach(trigger => trigger.update(status, this))
  }

  start() {
    this.updateStatus("running")
    var scriptPath = path.join(__dirname, "scripts", this.hook.script)
    execFile(scriptPath, {
      cwd: path.join(__dirname, "scripts")
    }, (error, stdout, stderr) => {
      if(error) {
        this.emit("stderr", stderr)
        this.updateStatus("failed")
      } else {
        this.emit("stdout", stdout)
        this.updateStatus("success")
      }
    })
  }

  cancel() {
    // TODO thing
  }
}

module.exports = Job